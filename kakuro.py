from itertools import permutations, combinations
import sys

def generate(number, boxes, known=[]):
  for combo in combinations(range(1, 10), boxes):
    if sum(combo) == number and all([x in combo for x in known]):
      yield combo

if __name__ == '__main__':
  while True:
    number = 0
    boxes = 0
    known = []
    try:
      entry = raw_input('number boxes [known1, ...]: ').split(' ')
      number = int(entry[0])
      boxes = int(entry[1])
      known = [int(x) for x in entry[2:]]
    except KeyboardInterrupt:
      print
      sys.exit(0)
    except:
      print "Invalid input"

    combos = list(generate(number, boxes, known))  
    for combo in combos:
      print combo

    flat = set([item for sublist in combos for item in sublist])
    print 'FLAT: ', flat

  