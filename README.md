Start by running `python kakuro.py`  
The prompt will then ask you for some data with the following pattern:  
`<number> <boxes> [<known1>, <known2>, ...]`  
- Where number is the number you're trying to find combination numbers for
- Boxes is in how many boxes you have to fill out the combination numbers in
- the rest of the numbers indicate which numbers you know for sure are on that line of boxes. (this reduces the possible combinations for some numbers)

Examples:
- **You have a row or column with 2 boxes for the number 4.**  
  Input: 4 2  
  Output: (1, 3)
- **You have a row or column with 3 boxes for the number 23.**  
  Output: (6, 8, 9)
- **You have a row or column with 4 boxes for the number 12.**  
  Input: 12 4  
  Output:  
  (1, 2, 3, 6)  
  (1, 2, 4, 5)
- **You have a row or column with 4 boxes for the number 12. But you know that one of the boxes is 4**  
  Input: 12 4 4  
  Output: (1, 2, 4, 5)